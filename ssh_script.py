from paramiko import client

class Ssh:
    client = None

    def __init__(self, host, port, username, password):

        print("Connecting to the host " + host + " with user: " + username + " and password: " + password + ".")
        self.client = client.SSHClient()
        self.client.set_missing_host_key_policy(client.AutoAddPolicy())
        self.client.connect(host, port=port,username=username, password=password, look_for_keys=False)
        print("Connection is established!")

    def sendCommand(self, command):
        # Check if connection was made previously
        if (self.client):
            stdin, stdout, stderr = self.client.exec_command(command)
            while not stdout.channel.exit_status_ready():
                if stdout.channel.recv_ready():
                    alldata = stdout.channel.recv(1024)
                    while stdout.channel.recv_ready():
                        
                        alldata += stdout.channel.recv(1024)

                    print(str(alldata))
            print("Connection to the given host was closed.")

        else:
            print("Connection were not established.")
