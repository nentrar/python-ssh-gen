# SSH Session Generator
Desktop application for generating ssh sessions with executing commands on the desired OS.

### How to use?

The application is divided by 3 sections:
* in the first enter details about the host: ip, port and credentials,
* in the second add commands You wanted to be executed and divide each by &&,
* (optional) choose "Multiple sessions" if You want to have more than 1. Specify the number in the next area. 

Click Start and wait. When all sessions are generated You will get popup with executed time included. 

When You close program the current filled data will be stored in the "sessionData" folder (created on Your drive when first run the application)
Next time when You want to run generator for those data, **first** click "Clear" and then "Restore previous session config". 